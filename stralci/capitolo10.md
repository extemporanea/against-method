---
layout: page
title: Capitolo 10
permalink: /stralci/capitolo10
---


# Selezione dal capitolo 10


#### Riassunto
_Neppure l’esperienza iniziale col cannocchiale offri tali ragioni. Le prime osservazioni telescopiche del cielo furono indistinte, indeterminate, contraddittorie e in conflitto con ciò che chiunque poteva vedere a occhio nudo. E la sola teoria che avrebbe potuto aiutare a separare le illusioni telescopiche da fenomeni veridici era confutata da test semplici._

<hr/>

#### [L'osservazione al telescopio produce risultati non riproducibili e la sua affidabilità è in discussione ]

[...] <mark>Un altro convegno che divenne noto in tutt’Europa chiarisce ancor più la situazione. Circa un anno prima, il 24 e 25 aprile 1610, Galileo aveva portato il cannocchiale a casa del suo oppositore, Giovanni Antonio Magini, a Bologna, per darne dimostrazione dinanzi a ventiquattro professori appartenenti a tutte le facoltà. Horky, l’ex allievo di Keplero che era fuori di sé per l’agitazione, scrisse in questa circostanza: “Il 24 e il 25 aprile non ho mai dormito, né di giorno né di notte, ma ho provato questo strumento di Galileo in mille volte mille modi (millies mille modis), tanto nelle cose inferiori come in quelle superiori. Nelle cose inferiori fa miracoli; in cielo fallisce, in quanto alcune stelle fisse si vedono doppie [sono menzionate, ad esempio, Spica, della Vergine, e una fiamma terrestre]… Ho come testimoni uomini eccellentissimi e dottori nobilissimi…; tutti hanno confessato che lo strumento erra. Galileo fu ridotto al silenzio e il lunedì 26 se ne andò malinconicamente di prima mattina… senza neppure ringraziare il signor Magini per il magnifico pranzo…”</mark> Il 26 maggio lo stesso Magini scrisse a Keplero: “Non ha ottenuto alcun risultato, poiché più di venti dotti uomini erano presenti; eppure[…]”

[...] <mark>Il carattere più strano degli inizi della storia del telescopio emerge, però, quando consideriamo attentamente le immagini che Galileo ci dà della Luna.
È sufficiente dare una rapida occhiata ai disegni di Galileo e alle fotografie di fasi simili per convincersi del fatto che “nessuno dei caratteri morfologici disegnati… può essere sicuramente identificato con alcun elemento noto del paesaggio lunare”.</mark>

Soltanto una nuova teoria della visione telescopica avrebbe potuto portare ordine nel caos (che avrebbe potuto essere ancora maggiore in conseguenza dei diversi fenomeni che si vedevano a quel tempo anche a occhio nudo) e separare l’apparenza dalla realtà. Una tale teoria fu sviluppata da Keplero, prima del 1604 e poi di nuovo nel 1611.
