---
layout: page
title: Capitolo 17
permalink: /stralci/capitolo17
---

# Capitolo 17


> [...]  se fissiamo regole specifiche per il processo di confronto, come le regole della logica nella loro applicazione al rapporto fra classi di contenuto, troveremo eccezioni, restrizioni indebite, e saremo costretti ogni volta a discutere una via d’uscita. È molto più interessante e istruttivo esaminare quali generi di cose si possano dire (rappresentare) e quali generi di cose non si possano dire (rappresentare) se il confronto deve aver luogo all’interno di una certa cornice o sistema concettuale (framework) specificamente e storicamente ben stabilito. Per un tale esame dobbiamo andare oltre le generalità e studiare i sistemi nei particolari.

> [...] L’argomentazione (che non può mai essere conclusiva) consiste nell’indicare caratteri tipici in campi lontani. Se i tratti peculiari di un particolare stile di pittura si riscontrano anche nella statuaria, nella grammatica di lingue coeve (specialmente in classificazioni nascoste che non possono essere facilmente distorte), se si può dimostrare che questi linguaggi sono propri agli artisti e al popolo comune, se ci sono principi filosofici formulati in tali linguaggi i quali dichiarano che tali peculiarità sono caratteri del mondo e non solo artefatti in esso presenti e cercano di spiegarne l’origine, se l’uomo e la natura posseggono questi caratteri non soltanto in pittura, ma anche nella poesia, in detti popolari, nel diritto consuetudinario, se l’idea che tali caratteri siano parte della percezione normale non è contraddetta da ciò che sappiamo dalla fisiologia, o dalla psicologia della percezione, se pensatori posteriori attaccano tali peculiarità come “errori” risultanti da un’ignoranza del “modo vero”, allora possiamo supporre di trovarci di fronte non semplicemente a limiti della capacità
tecnica e a intenzioni particolari, ma a un modo di vita coerente, e possiamo attenderci che la gente che partecipava di tale modo di vita vedesse il mondo nello stesso modo in cui noi vediamo ora le loro opere pittoriche. 
