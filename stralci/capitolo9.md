---
layout: page
title: Capitolo 9
permalink: /stralci/capitolo9
---


# Selezione dal capitolo 9


#### Riassunto
_Oltre a modificare interpretazioni naturali, Galileo cambia anche sensazioni che sembrano danneggiare Copernico. Egli ammette che sensazioni del genere esistano, elogia Copernico per averle trascurate, sostiene di averle eliminate con l’aiuto del cannocchiale. Non offre però alcuna ragione teorica per cui ci si dovrebbe attendere che il cannocchiale ci dia un’immagine fedele del cielo._

<hr/>

#### [La teoria Copernicana è in disaccordo con le osservazioni di Marte e Venere, ma questo non è un problema né per Copernico né per Galileo ]

[...] Un po’ più avanti <mark>Galileo osserva che Aristarco e Copernico hanno “confidato… in quello che la ragione gli dettava” e conclude che Copernico ha “costantemente continuato nell’affermare, scorto dalle ragioni, quello di cui le sensate esperienze mostravano il contrario:</mark> che io non posso finir di stupire ch’egli abbia pur costantemente voluto persistere in dir che Venere giri intorno al Sole, ed a noi sia meglio di sei volte più lontana una volta che un’altra, e pur sempre si mostri eguale a se stessa, quando ella dovrebbe mostrarsi quaranta volte maggiore".
<mark>Le esperienze che contraddicono apertamente il moto annuo di rivoluzione della Terra attorno al Sole, le quali sono ancora più evidenti e sensate delle argomentazioni dinamiche esposte sopra, consistono nel fatto che Marte “sarebbe necessario che quando è a noi vicinissimo si mostrasse il suo disco più di 60 volte maggiore di quello che si mostra quando è lontanissimo; tuttavia</mark> tal diversità di apparente grandezza non si scorge, anzi nella opposizione al Sole, <mark>quando è vicino alla Terra, non si mostra né anco 4 o 5 volte più grande che quando, verso la congiunzione, viene occultato sotto i raggi del Sole”.
> “Altra e maggior difficultà ci fa Venere, che se girando intorno al Sole, come afferma il Copernico, gli fusse ora sopra ed ora sotto, allontanandosi ed appressandosi a noi quanto verrebbe ad esser il diametro del cerchio da lei descritto, quando fusse sotto il Sole a noi vicinissima, dovrebbe il suo disco mostrarcisi poco meno di 40 volte maggiore che quando è superiore al Sole, vicina all’altra sua congiunzione; tutta via la differenza è quasi impercettibile."</mark>

[...] Vediamo di nuovo che l’opinione di Galileo sull’origine del copernicanesimo differisce in misura notevole dalle esposizioni storiche che ci sono più familiari. Egli non sottolinea _fatti nuovi_ che offrano un _sostegno_ induttivo all’idea di una Terra in movimento, né menziona osservazioni in grado di _confutare_ il punto di vista geocentrico e di essere invece spiegate dal copernicanesimo. <mark>Galileo insiste, al contrario, sul fatto che non soltanto Tolomeo, ma anche Copernico, è confutato dai fatti ed elogia Aristarco e Copernico per non aver rinunciato di fronte a difficoltà tanto grandi. Li elogia per aver proceduto _controinduttivamente_.</mark>
Questa non è però ancora l’intera storia.

<hr/>

#### [Il telescopio viene introdotto da Galileo senza una teoria che ne spieghi in maniera soddisfacente il funzionamento]

[...] <mark>Questi ammette che, “se senso superiore e più eccellente de i comuni e naturali non si accompagnava alla ragione”, egli sarebbe stato “assai più ritroso contro al sistema Copernicano”.</mark> Il “senso superiore e più eccellente” è ovviamente il _telescopio_, e si è inclini a rilevare che il procedimento chiaramente controinduttivo era un’induzione di fatto (o una congettura più una confutazione più una nuova congettura), _ma un’induzione fondata su un’esperienza migliore_, contenente non solo interpretazioni naturali migliori ma anche un nucleo di materiali sensibili migliori rispetto ai materiali d’osservazione disponibili ai predecessori aristotelici di Galileo. Ora dobbiamo esaminare questi argomenti un po’ più nei particolari.
<mark>Il telescopio è un “senso superiore e più eccellente” che ci offre materiali di osservazione nuovi e più attendibili per giudicare in materia di astronomia. In che modo viene esaminata quest’ipotesi e quali argomenti vengono presentati in suo favore?”</mark>

[...] <mark>esistono seri dubbi sulla conoscenza, da parte di Galileo, di quelle parti dell’ottica fisica contemporanea che erano rilevanti per la comprensione dei fenomeni telescopici.</mark>
[...] Galileo considerava l’argomento molto difficile e che aveva trovato la Diottrica di Keplero, pubblicata nel 1611, “così oscuro [il libro], che forse nemmeno l’autore l’aveva capito”. In una lettera a Liceti, scritta due anni prima della sua morte, Galileo osservava che, quanto alla natura della luce, egli era ancora nelle tenebre.”
<mark>Provando e riprovando: ciò significa che “nel caso del cannocchiale, fu l’esperienza e non la matematica a condurre Galileo alla sua grande conquista: la conquista della fiducia nella veridicità dell’apparecchio”</mark>.
