---
layout: page
title: Capitolo 3
permalink: /stralci/capitolo3
---

# Selezione dal Capitolo 3

#### Riassunto
> _La condizione della coerenza, la quale richiede che le nuve ipotesi siano in accordo con teorie accettate, è irragionevole, in quanto preserva la teoria anteriore, non la teoria migliore. Le ipotesi in contraddizione con teorie ben affermate ci forniscono materiali di prova che non possono essere ottenuti in alcun altro modo. La proliferazione delle teorie è benefica per la scienza, mentre l’uniformità ne menoma il potere critico. L’uniformità danneggia anche il libero sviluppo dell'individuo.

<hr/>

#### [Riassumendo la condizione di coerenza ]

> [...] Val la pena di ripetere il nucleo ragionevole di quest’argomentazione. Le teorie non dovrebbero essere cambiate se non quando ci siano ragioni pressanti per farlo. L’unica ragione pressante per modificare una teoria è il disaccordo con i fatti. La discussione di fatti incompatibili con una teoria condurrà perciò al progresso; non condurrà invece a progressi la discussione di ipotesi incompatibili. Un procedimento sano è quindi quello di accrescere il numero dei fatti rilevanti, mentre non è un buon procedimento quello di aumentare il numero di alternative adeguate sì ai fatti ma incompatibili con la teoria corrente. Piacerebbe aggiungere che non dovrebbero essere esclusi miglioramenti formali, come una maggiore eleganza, semplicità, generalità e coerenza; ma, una volta che questi miglioramenti siano stati eseguiti, l’accumulo di fatti ai fini della verifica sperimentale pare sia l’unica
possibilità che rimane allo scienziato.[...]

<hr />

#### [Sul "principio di coerenza"]

> [...] Esso afferma però che i fatti che appartengono al contenuto empirico di una qualche teoria sono disponibili vengano o no prese in considerazione alternative a tale teoria. [...] Ammetto che questo è un quadro troppo semplice della situazione reale. Fatti e teorie sono connessi in modo molto più intimo di quanto non ammetta il principio di autonomia. Non soltanto la descrizione di ciascun fatto singolo dipende da qualche teoria (la quale potrebbe, ovviamente, essere molto diversa dalla teoria che dev’essere verificata), ma esistono anche fatti che non possono emergere se non con l’aiuto di alternative alla teoria che si tratta di verificare, e che cessano di essere disponibili non appena tali alternative siano escluse. Ciò suggerisce che l’unità metodologica alla quale dobbiamo riferirci quando discutiamo questioni di verifica e di contenuto empirico è costituita da un intero insieme di teorie, in parte sovrapponentisi, fattualmente adeguate ma reciprocamente contraddittorie



> [...] l’invenzione e l’elaborazione di alternative potrebbero dover precedere la produzione di fatti destinati a confutare una teoria. L’empirismo, almeno in alcune fra le sue versioni più sofisticate, richiede che il contenuto empirico di qualsiasi conoscenza da noi posseduta venga accresciuto il più possibile. Perciò l’invenzione di alternative all’opinione in esame costituisce una parte essenziale del metodo empirico.

>[...] Ora, se è vero, come abbiamo sostenuto nel capoverso
precedente, che molti fatti diventano disponibili solo con l’aiuto di teorie
alternative, allora il rifiuto di considerare queste ultime avrà come
conseguenza l’eliminazione anche di fatti suscettibili di confutare la
teoria accettata. Più specificamente, tale rifiuto condurrà all’eliminazione
di fatti la cui scoperta avrebbe dimostrato la completa e irrimediabile inadeguatezza della teoria.

#### [Sul successo di una teoria a discapito delle alternative]

> Nello stesso tempo è evidente, sulla base delle nostre considerazioni, che quest’apparenza di successo non può affatto essere considerata un segno di verità e di corrispondenza con la natura. Al contrario, sorge il sospetto che l’assenza di grandi difficoltà sia proprio il risultato della diminuzione di contenuto empirico determinata dall’eliminazione delle teorie alternative e dei fatti che potrebbero essere scoperti solo col loro aiuto. In altri termini, sorge il sospetto che questo presunto successo sia dovuto al fatto che la teoria, quando fu estesa oltre il suo punto di partenza, fu trasformata in
una rigida ideologia.

> [...] Per concludere: l’unanimità di opinione può essere adatta per una chiesa, per le vittime atterrite o bramose di qualche mito (antico o moderno), e per i seguaci deboli e pronti di qualche tiranno. Per una conoscenza obiettiva è necessaria la varietà di opinione. E un metodo che incoraggi la varietà è anche l'unico metodo che sia compatibile con una visione umanitaria. (Nella misura in cui la condizione di coerenza limita la varietà, essa contiene un elemento teologico, il quale risiede, ovviamente, nel culto dei “fatti” che è così tipico di quasi tutte le forme di empirismo”).
