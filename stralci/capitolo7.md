---
layout: page
title: Capitolo 7
permalink: /stralci/capitolo7
---


# Selezione dal capitolo 7

#### Riassunto
>Le nuove interpretazioni naturali costituiscono un linguaggio d’osservazione nuovo e altamente astratto. Esse sono introdotte tacitamente, così che non si rilevi il mutamento che ha avuto luogo (metodo dell'anamnesi). Esse contengono l’idea della relatività di ogni moto e la legge dell'inerzia circolare.



> In che modo [Galileo] riesce a introdurre asserzioni assurde e controinduttive, come l'asserzione che la Terra si muove, procurando nondimeno loro un ascolto giusto e attento? Ci si immagina immediatamente che le argomentazioni da sole non bastino - ecco qui una limitazione interessante e molto importante del razionalismo - e i discorsi di Galileo sono in effetti argomentazioni solo in apparenza. Galileo si serve infatti dei mezzi della propaganda. Oltre a tutte le ragioni intellettuali che può offrire egli fa ricorso anche a trucchi psicologici. Questi trucchi hanno molto successo e lo conducono alla vittoria. Essi oscurano però il nuovo atteggiamento nei confronti dell'esperienza in divenire e procrastinano per secoli la possibilità di una filosofia ragionevole. Essi oscurano il fatto che l'esperienza su cui Galileo vuol fondare la concezione copernicana non è altro che il risultato della sua fertile immaginazione, che è un'esperienza inventata. Essi oscurano questo fatto insinuando che i nuovi risultati che emergono siano noti e concessi da tutti e che abbiano bisogno solo di richiamare su di sé la nostra attenzione per apparirci come l'espressione più ovvia della verità.

> [...] La resistenza contro l’assunto che un moto comune non opera viene considerata tutt’uno con la resistenza che idee dimenticate oppongono al tentativo di richiamarle alla memoria. Accettiamo pure questa interpretazione della resistenza, ma non dimentichiamo che la resistenza esiste. Dobbiamo ammettere allora che questa resistenza restringe l’uso di idee relativistiche, limitandole a una parte della nostra esperienza quotidiana. All’esterno di questa parte, ossia nello spazio interplanetario, esse sono “dimenticate” e perciò non sono attive. Ma all’esterno di tale ambito non c’è un caos completo. Altri concetti vengono usati e fra questi ci sono quei medesimi concetti assolutistici che derivano dal primo paradigma.
Non soltanto li usiamo, ma dobbiamo ammettere che sono del tutto adeguati. Nessuna difficoltà insorge finché si rimane nei limiti del primo paradigma. L’“esperienza”, ossia la totalità di tutti i fatti provenienti da qualsiasi campo, non può costringerci a eseguire il mutamento che Galileo desidera introdurre. Il motivo per un mutamento deve venire da una fonte diversa.

> [...] Esso viene dal “bisogno tipicamente metafisico” dell’unità di comprensione e di presentazione concettuale E il motivo per un mutamento è connesso, in secondo luogo, con l’intenzione di fare spazio al moto della Terra, che Galileo accetta e al quale non è disposto a rinunciare.

> [...] L’idea dell'anamnesi svolge qui la funzione di un sostegno psicologico, di una leva destinata a facilitare questo processo di sussunzione occultandone l’esistenza.
