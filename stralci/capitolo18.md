---
layout: page
title: Capitolo 18
permalink: /stralci/capitolo18
---


# Selezione dal capitolo 18


#### Riassunto
_La scienza è quindi molto più vicina al mito di quanto una filosofia scientifica sia disposta ad ammettere. <mark>Essa è una fra le molte forme di pensiero che sono state sviluppate dall’uomo, e non necessariamente la migliore</mark>. È vistosa, rumorosa e impudente, ma è intrinsecamente superiore solo per coloro che hanno già deciso a favore di una certa ideologia, o che l’hanno accettata senza aver mai esaminato i suoi vantaggi e i suoi limiti. E poiché l’accettazione e il rifiuto di ideologie dovrebbero essere lasciati all’individuo, ne segue che la separazione di stato e chiesa dovrebbe essere integrata dalla separazione di stato e scienza, che è la più recente, la più aggressiva e la più dogmatica istituzione religiosa. Una tale separazione potrebbe essere la nostra unica possibilità di conseguire un’umanità di cui siamo capaci, ma che non abbiamo mai realizzato compiutamente._

<hr/>

#### [La scienza come ideologia.]

[...] <mark>Infine, il modo in cui noi accettiamo o ripudiamo idee scientifiche è radicalmente diverso dai procedimenti decisionali democratici. Accettiamo leggi scientifiche e fatti scientifici, li insegniamo nelle nostre scuole, ne facciamo la base di importanti decisioni politiche, ma senza averli mai sottoposti a una votazione. Gli scienziati non li sottopongono al voto — o almeno così dicono — e certamente ciò vale a maggior ragione per i profani. Talvolta vengono discusse proposte concrete e viene suggerito di procedere a una votazione. Il procedimento non viene esteso però a teorie generali e a fatti scientifici.</mark>

[...] Non esiste alcun metodo speciale che garantisca il successo o lo renda probabile. Gli scienziati risolvono i problemi non perché posseggano una bacchetta magica — una metodologia, o una teoria della razionalità — ma perché hanno studiato un problema per molto tempo, perché conoscono abbastanza bene la situazione, perché non sono troppo stupidi (anche se oggi c’è il sospetto che quasi tutti potrebbero diventare scienziati) e perché gli eccessi di una scuola scientifica sono controbilanciati quasi sempre dagli eccessi di qualche altra scuola. (Solo di rado, inoltre, gli scienziati risolvono i loro problemi; spesso commettono quantità di errori e molte fra le loro soluzioni sono del tutto inutili.)
<mark>Sostanzialmente c’è ben poca differenza fra il processo che conduce all’annuncio di una nuova legge scientifica e il processo che precede l’approvazione di una nuova legge nella società: si informano o tutti i cittadini o le persone direttamente interessate, si raccolgono “fatti” e pregiudizi, si discute l’argomento e infine si vota. Ma mentre una democrazia fa qualche sforzo per _spiegare_ il processo, in modo che tutti possano capirlo, gli scienziati o lo _nascondono_, o lo _piegano_ per adattarlo ai loro interessi settari.
Nessuno scienziato ammetterà che il voto svolga una funzione nel suo campo. A decidere sono solo i fatti, la logica e la metodologia: questo ci dice la storia raccontata dagli scienziati. Ma in che modo decidono i fatti? Qual è la loro funzione nel progresso della conoscenza?</mark>

<hr/>

#### [La scienza è raccontata come non democratica ai profani ma nei fatti lo è, e questa dissonanza permette a particolari interessi di prevalere.]

[...] <mark>Vediamo: i fatti da soli non sono abbastanza forti da farci accettare, o rifiutare, teorie scientifiche, e il campo che essi lasciano al pensiero è _troppo vasto_; la logica e la metodologia eliminano troppo, sono _troppo ristrette_. Fra questi due estremi è compreso l’ambito sempre mutevole delle idee e dei desideri umani. E una analisi più particolareggiata delle mosse che hanno successo nella partita della scienza (che “hanno successo” dal punto di vista degli scienziati stessi) dimostra in effetti l’esistenza di un ampio ambito di libertà che _esige_ una molteplicità di idee e _permette_ l’applicazione di procedimenti democratici (discussione democratica e voto), ma che di fatto è chiuso dal potere politico e dalla propaganda. _Proprio a questo punto la favola di un metodo speciale assume la sua funzione decisiva._ Esso occulta, mediante un’esposizione di criteri “oggettivi”, la libertà di decisione che gli scienziati creativi e il pubblico generale hanno anche all’interno delle parti più rigide e più avanzate della scienza,</mark> [...]

[...] <mark>In questo modo gli scienziati hanno ingannato se stessi e tutti gli altri sulla loro attività</mark> [...]
