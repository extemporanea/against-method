---
layout: page
title: Capitolo 11
permalink: /stralci/capitolo11
---


# Selezione dal capitolo 11


#### Riassunto
_Ci sono, d'altra parte, alcuni fenomeni telescopici che si rivelano chiaramente copernicani. Galileo introduce questi fenomeni come prove indipendenti a favore di Copernico, mentre la situazione vera è piuttosto che una concezione confutata — il copernicanesimo — ha una certa somiglianza con fenomeni emergenti da un’altra concezione confutata: l’idea che i fenomeni telescopici siano immagini fedeli del cielo. Galileo si impone grazie al suo stile e alle sue capacità di persuasione, perché scrive in italiano anziché in latino e perché si appella a persone che si oppongono per temperamento alle vecchie idee e ai modelli di insegnamento ad esse connessi._

<hr/>

#### [Osservazioni al telescopio e teoria copernicana sono in accordo nonostante i rispettivi problemi]

[...] <mark>Ma — e arriviamo così a quello che mi pare il carattere centrale del procedimento di Galileo — ci sono _fenomeni telescopici_, in particolare la variazione telescopica della luminosità dei pianeti, _che si accordano con Copernico più che non facciano i risultati dell’osservazione a occhio nudo_. Visto al cannocchiale, Marte muta in effetti come dovrebbe secondo la concezione copernicana. Messa a confronto con la prestazione totale del cannocchiale, questa variazione rimane ancora sconcertante. Essa è sconcertante qual era nella teoria copernicana quando veniva messa a confronto con i dati d’osservazione pretelescopici. La variazione è però in accordo con le predizioni di Copernico.</mark> _È quest'accordo_, più che una profonda comprensione della cosmologia e dell’ottica, _a dimostrare per Galileo la verità della teoria di Copernico e la veridicità del cannocchiale_ in terra _e_ in cielo. E su quest’armonia egli costruisce una visione del tutto nuova dell’universo.

[...] <mark>Questi sfrutta proprio questa situazione piuttosto peculiare, questo accordo fra due idee interessanti ma confutate allo scopo di impedire l’eliminazione di entrambe.</mark>

[...] <mark>Il lettore si renderà conto che uno studio più particolareggiato di fenomeni storici come questi crea difficoltà considerevoli all’opinione che il passaggio dalla cosmologia precopernicana a quella del Seicento sia consistito semplicemente nella sostituzione di teorie confutate con congetture più generali, che spiegavano i casi contraddittori con le vecchie teorie, facevano nuove predizioni ed erano corroborate da osservazioni eseguite per verificare queste nuove predizioni. Il lettore si renderà forse conto dei meriti di un’opinione diversa, secondo la quale, se l’astronomia precopernicana _si trovava in difficoltà_ (posta di fronte com’era a una serie di dati d’osservazione che la confutavano e a numerose implausibilità), la teoria copernicana _versava in difficoltà ancora maggiori_ (trovandosi di fronte a dati d’osservazione che la confutavano e a implausibilità ancora più drastiche); ma, essendo in accordo _con teorie ancor più inadeguate_, guadagnò forza e fu conservata, le confutazioni essendo state rese inefficaci mediante ipotesi _ad hoc_ e mediante tecniche di persuasione più abili. Questa parrebbe una descrizione degli sviluppi che ebbero luogo al tempo di Galileo molto più adeguata di quella offerta da quasi tutte le esposizioni alternative.</mark>
