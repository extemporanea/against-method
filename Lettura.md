---
layout: page
title: Lettura
permalink: /lettura/
---
# Libera Lettura a Festivaletteratura 2022

### Struttura

- (10-20 min) Domande provocatorie all'inizio: il pubblico risponde e dibatte liberamente, e noi si è solo *facilitatori*.
  - In che modo Galileo sostenne la visione copernicana del moto della Terra? 
  - C'è un posto per la narrazione e la propaganda nella scienza e nel suo sviluppo? [qui contributo di Matteo su termodinamica come esempio]
  - Che cosa vuol dire "credere nella scienza"?
  - Chi è una scienziata o scienziato oggi? che differenza c'è col passato ? e col futuro? 
- (5-10min) Presentazione di Feyerabend [Mattia]
- (15-20 min lettura) Passaggi scelti (dalla [preselezione](/against-method/stralci)) [nell'ordine Fulvio/Francesco/Mattia]
- (20 min) Commenti 
- (20 min) Discussione col pubblico

### Preparazione social

Una settimana prima

- presentazione sulla newsletter
- uso dei canali social per la promozione (telegram/instagram)

### Impressioni

[Impressioni di Francesco](/against-method/impressioni)
