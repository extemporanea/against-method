---
layout: page
title: Bibliografia
---

# Edizioni di riferimento


#### In inglese:

Paul Feyerabend, _Against Method_, Verso (2010).
[&rarr; Link all'Editore ](https://www.versobooks.com/books/442-against-method)


#### In  italiano:
Paul K. Feyerabend, _Contro il metodo_, Feltrinelli (2002).
[&rarr; Link all'Editore ](https://www.lafeltrinelli.it/contro-metodo-abbozzo-di-teoria-libro-paul-k-feyerabend/e/9788807882333)

# Critica

Bruno Latour, _The Pasteurization of France_, Harvard University Press (1993).
[&rarr; Link all'Editore ](https://www.hup.harvard.edu/catalog.php?isbn=9780674657618)

Bruno Latour, _Non siamo mai stati moderni_, Eleuthera (2018).
[&rarr; Link all'Editore ](https://eleuthera.it/scheda_libro.php?idlib=469)

